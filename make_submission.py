import pandas as pd
import subprocess
from glob import glob
def make_submission(data, desc=''):
    df1 = pd.read_csv('tmlcc_dataset/test.csv')
    submission = pd.DataFrame({
        "id": [i.split('_')[-1] for i in df1['MOFname'].to_list()],
        "CO2_working_capacity [mL/g]": data.flatten()
    })

    submission.to_csv("submission.csv", index = False)
    filename = 'submission_phatham_%03d_%s.zip' % (len(glob('submission*.zip')) + 1, desc)
    subprocess.run(['zip', filename, 'submission.csv'])
    return filename