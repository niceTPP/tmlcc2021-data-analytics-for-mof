from glob import glob
import numpy as np
import os
dataset_list = glob('tmlcc_dataset/mof_cif_train/*.cif')
dataset_list += glob('tmlcc_dataset/mof_cif_pretest/*.cif')
dirs = {'/'.join(i.split('/')[:-1]) for i in dataset_list}
params = [
    '-resex'
]
for i in np.arange(0.25, 3.001, 0.25):
    params.append('-chan %.2f' % i)
    params.append('-psd %.2f %.2f 1000' % (i, i))
    for pref in ['vol', 'volpo', 'sa']:
        params.append('-%s %.2f %.2f 5000' % (pref, i, i))
        
def cmdParam(fname, param_name):
    return './shared/zeopp/network -ha ' + param_name + (' %s/%s.%s %s' % (param2dir(param_name), fname, param2ext(param_name), fname))
def cmds(fname):
    result_l = []
    for i in params:
        full_cmd = cmdParam(fname, i)
        result_l.append(full_cmd)
    return result_l
def param2ext(param_name):
    return param_name.split(' ')[0][1:] if ' ' in param_name else param_name[1:]
def param2dir(param_name):
    if ' ' in param_name:
        return 'zeoout/' + param_name.split(' ')[0][1:] + '_' + param_name.split(' ')[1]
    return 'zeoout/' + param_name[1:]

for dd in [param2dir(i) for i in params]:
    for i in dirs:
        os.makedirs('%s/%s' % (dd, i), exist_ok=True)

import multiprocessing
import subprocess
import shlex

from multiprocessing.pool import ThreadPool

def call_proc(cmd):
    """ This runs in a separate thread. """
    #subprocess.call(shlex.split(cmd))  # This will block until cmd finishes
    p = subprocess.Popen(shlex.split(cmd), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = p.communicate()
    return (out, err)


for param in params:
    pool = ThreadPool(multiprocessing.cpu_count())
    for fname in dataset_list:
        pool.apply_async(call_proc, (cmdParam(fname, param),))
    pool.close()
    pool.join()
