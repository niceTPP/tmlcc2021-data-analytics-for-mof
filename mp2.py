from ase.io import read
from collections import Counter
from glob import glob
import multiprocessing
import numpy as np
import pickle
import itertools
import os
dsets = sorted(glob('tmlcc_dataset/*/*.cif'))
#dsets = []
#for f1 in dsets1:
#    outfile = 'spect/' + f1 + '.npy'
#    if os.path.isfile(outfile) and np.load(outfile).shape == (16, 1001):
#        continue
#    dsets.append(f1)
    
R = np.linspace(0.25, 100.25, 1001).reshape(-1, 1, 1)

with open('props.pkl', 'rb') as fp:
    props = pickle.load(fp)

def process_spect(f):
    outfile = 'spect/' + f + '.npy'
    if os.path.isfile(outfile) and np.load(outfile).shape == (16, 1001):
        return 0
    atoms = read(f)
    chem_len = len(atoms.get_chemical_symbols())
    mask = 1 - np.tril(np.ones(chem_len))
    symbols = atoms.get_chemical_symbols()
    prop_mat_masked = {k: mask * np.array([
        props[k][i] for i in itertools.product(symbols, symbols)
    ]).reshape(chem_len, chem_len) for k in props}
    B = 100
    prop_mat_masked_keys = sorted(prop_mat_masked)
    spect_b = np.zeros((16, 1001))
    ii = 0
    for mic_op in [True, False]: # minimum image convention
        dist_mat = atoms.get_all_distances(mic=mic_op)
        mult = np.exp(-B*np.power((dist_mat - R), 2))
        for k in prop_mat_masked_keys: # 8
            spect_b[ii, :] = np.sum(prop_mat_masked[k] * mult, (1, 2))
            ii += 1
    np.save(outfile, spect_b)
    return 0

ci = 0
with multiprocessing.Pool() as pool:
    for finn in pool.imap_unordered(process_spect, dsets):
        ci += 1
        print(ci, len(dsets))
#np.save('all_spectr.npy', results)
#with open('dset_name.pkl', 'wb') as fp:
#    pickle.dump(dsets, fp)

